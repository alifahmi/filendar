[ $# -lt 4 ] && echo "Usage: $0 filepath station keyword start_year end_year" && exit 1

dir=$1
sta=$2
key=$3
startyear=$4
endyear=$5
scriptpath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )" 

#for sta in $(ls $dir);
#do 
for yr in $(seq $startyear $endyear); 
do 
	for mo in {01..12}; 
	do 
		$scriptpath/script.sh $dir $sta $key $yr $mo
	done

	$scriptpath/combine.sh $sta $yr
done
#done
