# filendar
Create hourly seismic (.gcf) file availability in a monthly/yearly calendar format.
- `script.sh` : create monthly data calendar.
- `combine.sh` : combine the script.sh result.
- `filendar.sh` : do both script above in one step. It only asks for the main path. For different file path, edit the count variable in script.sh file.


**File path format:**

`/filepath/STA/yyyy/yyyy-mm-dd/yyyymmdd_HHMM_sssscc.fff`

`STA   = station name, e.g. MELEK`

`yyyy  = year`

`mm    = month`

`dd    = day`

`HH    = hour`

`MM    = minutes`

`ssss  = serial number, e.g. 4y88`

`cc    = channel, e.g. {e|n|z}2`

`fff   = file format, e.g. gcf`

**Sample**

`/DATA/Merapi/GCF/MELEK/2022/2022-09-23/20220923_0100_6c7dz2.gcf`

**Usage:**

`./filendar.sh filepath station keyword start_year end_year`

- `filepath` : path of main directory containing station directory
- `station`  : station name, e.g. MELEK
- `keyword`  : string used to find file based its name/serial number + channel
- `start_year`, `end_year` : input the same year to process 1 year of data

**Sample:**

`./filendar.sh /DATA/MERAPI/GCF MELEK 6c7dz 2021 2022`
